# Use middleware to handle multiple languages

## Prerequisites

You need to install Nextjs (doc [here](https://nextjs.org/)) and @formatjs/intl-localematcher (doc [here](https://www.npmjs.com/package/@formatjs/intl-localematcher)).

## Middleware

```ts
import { NextResponse, NextRequest } from "next/server";
import { match } from "@formatjs/intl-localematcher";

const locales = ["en", "fr"];
const defaultLocale = "en";
const localeSupported = {
  en: "en-US",
  fr: "fr-FR",
};

function redirectToDefaultLocale(request: NextRequest, path: string) {
  const browserLanguages = request.headers.get("Accept-Language");

  if (!browserLanguages) {
    return NextResponse.redirect(
      new URL(`/${defaultLocale}/${path}`, request.url)
    );
  }

  const acceptedLanguages = browserLanguages
    .split(",")
    .map((lang) => lang.split(";")[0]); // exampes: "en-US,en;q=0.9,fr;q=0.8" becomes ["en-US", "en", "fr"]
  const locale = match(acceptedLanguages, locales, defaultLocale);
  return NextResponse.redirect(new URL(`/${locale}/${path}`, request.url));
}

export function middleware(request: NextRequest) {
  const pathname = request.nextUrl.pathname;

  // Check if there is no locale in the pathname
  if (pathname === "/") {
    return redirectToDefaultLocale(request, "");
  }

  //Check if there is any supported locale in the pathname
  const pathnameArray = pathname.split("/"); // exampes: /en becomes ["", "en"], /en/something becomes ["", "en", "something"]

  // There is no locale in the pathname, so we redirect to default locale with the same path
  if (pathnameArray[1].length !== 2) {
    return redirectToDefaultLocale(request, pathname);
  }

  // There is a locale in the pathname. Check if the locale is supported. If so, do nothing. If not, redirect to default locale with the same path
  if (!(<any>Object).values(localeSupported).includes(pathnameArray[1])) {
    const path = pathnameArray.slice(2).join("/"); // exampes: /en becomes "", /en/something becomes "something", /en/something/else becomes "something/else"
    return redirectToDefaultLocale(request, path);
  }
}

export const config = {
  matcher: ["/((?!api|_next/static|_next/image|favicon.ico|images).*)"],
};
```