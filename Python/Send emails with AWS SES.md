# Send emails with AWS SES

## Prerequisites

You need to install boto3 (doc [here](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)). You also need to configure an email address in AWS SES. It is very easy. AWS will send you a secret code to make sure you own the email address. Once you have done that, you can use the email address to send emails.

## Send email

```py
import boto3

email_client = boto3.client("ses")
YOUR_EMAIL = "your_email@domain.com"

def send_email(receiver: list[str], subject: str, content: str) -> None:
    email_client.send_email(
        Source=YOUR_EMAIL,
        Destination={
            "BccAddresses": receiver,
        },
        Message={
            "Subject": {
                "Data": subject,
            },
            "Body": {
                "Text": {
                    "Data": content,
                },
                "Html": {
                    "Data": content,
                },
            },
        },
    )
```

## Pitfalls

The variable `receiver` is a list of email addresses. If you want to send an email to a single email address, you still need to put the email address in a list. 

And note that there is a limit of 50 recipients per email. If you want to send an email to more than 50 recipients, you need to send multiple emails (you can use a `for` loop). something like this :

```py
batch_size = 50
for i in range(0, len(email_addresses), batch_size):
    email_addresses_batch = email_addresses[i : i + batch_size]
    # send emails here
```

If your content is a simple text but you still want to send it as HTML, you can use this :
    
```py
"Data": "<br />".join(content.split("\n")),
```