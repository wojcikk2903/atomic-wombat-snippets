# Get password from AWS SecretsManager

## Prerequisites

You need to store a password in AWS SecretsManager (doc [here](https://aws.amazon.com/secrets-manager/)) and you need to install boto3 (doc [here](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)).

## Get password

```py
REGION = "eu-central-1"  # Replace this by your AWS region

def get_password(secretId):
    session = boto3.session.Session()
    client = session.client(service_name="secretsmanager", region_name=REGION)

    get_secret_value_response = client.get_secret_value(
        SecretId=secretId
    )

    # Return password from secret
    secret = get_secret_value_response["SecretString"]
    password = json.loads(secret)["password"]
    return password

# Usage example
password = get_password("my-secret-id")
```

## Pitfalls

Make sure to use the secret ID and not the secrect NAME. Both are very similar but the secret ID is the one you need.