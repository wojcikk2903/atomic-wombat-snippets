# Autocomplete cities field (with GoogleMaps API)

## Prerequisites

You need to install Material UI (doc [here](https://material-ui.com/)) and you need a GoogleMaps API Key (doc [here](https://developers.google.com/maps/documentation/places/web-service/autocomplete)) so that you can tailor this snippet to your needs.


## Component

```jsx
"use client";

import {
  Autocomplete,
  TextField,
} from "@mui/material";
import { useState, useEffect } from "react";
import { debounce } from "@mui/material/utils";

export function PlacesAutocomplete() {
  const [inputValue, setInputValue] = useState<string>("");
  const [placeOptions, setPlaceOptions] = useState<string[]>([]);
  const locale = "en";  // Or your locale
  const country = "be"; // Or your country

  const getGoogleMapsPlaces = async () => {
    if (inputValue === "") {
      setPlaceOptions([]);
      return;
    }
    fetch( `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${inputValue}&language=${locale}&components=country:${country}&types=locality|sublocality&key=${process.env.GOOGLE_MAPS_API_KEY}`)
      .then((response) => response.json())
      .then((results) => setPlaceOptions(results.predictions.map((place) => place.description)));
  };

  useEffect(() => {
    debounce(getGoogleMapsPlaces, 400)();
  }, [inputValue]);

  return (
    <Autocomplete
        disablePortal
        disableCloseOnSelect
        multiple
        filterSelectedOptions
        options={placeOptions}
        noOptionsText="No location found"
        renderInput={(params) => (
        <TextField {...params} label="Add a location" />
        )}
        value={places}
        onChange={(event: any, newValue: string[]) => {
            setPlaceOptions([]);
            setPlaces(newValue);
        }}
        inputValue={inputValue}
        onInputChange={(event, newInputValue) => {
            setInputValue(newInputValue);
        }}
    />
  );
}
```


## Pitfalls

Make sure to properly hide your Google Maps API key. In Next.js for example, you can call a serverless endpoint from your component to interact with the Google Maps API.
