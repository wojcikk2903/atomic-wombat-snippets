# Click on a button when Enter key is pressed

## Prerequisites

You need a button with an aria-label attribute.

## Snippet

```typescript
const clickButtonWhenEnterPressed =
  (ariaLabel: string) => (event: React.KeyboardEvent) => {
    if (event.key === "Enter") {
      (
        document.querySelector(
          `button[aria-label='${ariaLabel}']`
        ) as HTMLButtonElement
      ).click();
    }
  };

// Usage example
<input onKeyDown={clickButtonWhenEnterPressed("My button label")} />
```