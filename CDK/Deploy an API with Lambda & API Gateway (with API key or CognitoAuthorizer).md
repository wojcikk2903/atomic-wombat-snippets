# Deploy an API with Lambda & API Gateway (with API key or CognitoAuthorizer)

## Prerequisites

You need an API ready to be deployer. The directory should be named `my_lambda` and contain a subdirectory called `app`. Inside the subdirectory, there should be a `main.py` file with a `handler` function. Feel free to use any other structure, but make sure to update the snippet below.

## API with no authentication

```py
from aws_cdk import aws_apigateway as apigateway
from aws_cdk import aws_lambda as lambda_

function = lambda_.Function(
    self,
    "MyLambda",
    function_name="my-lambda",
    runtime=lambda_.Runtime.PYTHON_3_10,
    handler="app.main.handler",  # Or the path to your handler function
    code=lambda_.Code.from_asset("./my_lambda"),  # Or the path to your code folder
    timeout=Duration.seconds(30),
    architecture=lambda_.Architecture.ARM_64,
    memory_size=1024,
)

api = apigateway.LambdaRestApi(
    self,
    "MyApi",
    rest_api_name="my-api",
    handler=function,
    proxy=True,
    integration_options=apigateway.LambdaIntegrationOptions(
        allow_test_invoke=False,
    ),
)

function.grant_invoke(iam.ServicePrincipal("apigateway.amazonaws.com"))
```

## API with API key

```py
from aws_cdk import aws_apigateway as apigateway
from aws_cdk import aws_lambda as lambda_

function = lambda_.Function(
    self,
    "MyLambda",
    function_name="my-lambda",
    runtime=lambda_.Runtime.PYTHON_3_10,
    handler="app.main.handler",  # Or the path to your handler function
    code=lambda_.Code.from_asset("./my_lambda"),  # Or the path to your code folder
    timeout=Duration.seconds(30),
    architecture=lambda_.Architecture.ARM_64,
    memory_size=1024,
)

api = apigateway.LambdaRestApi(
    self,
    "MyApi",
    rest_api_name="my-api",
    handler=function,
    proxy=True,
    integration_options=apigateway.LambdaIntegrationOptions(
        allow_test_invoke=False,
    ),
    default_method_options=apigateway.MethodOptions(
        api_key_required=True,
    ),
)

key = api.add_api_key(
    "my-api-key", api_key_name="my-api-key"
)

plan = api.add_usage_plan(
    "my-usage-plan",
    name="my-usage-plan",
    quota=apigateway.QuotaSettings(
        limit=1000,
        period=apigateway.Period.DAY,
    ),
)

plan.add_api_key(key)
plan.add_api_stage(
    stage=api.deployment_stage,
)

function.grant_invoke(iam.ServicePrincipal("apigateway.amazonaws.com"))
```

## API with Cognito Authorizer

```py
from aws_cdk import aws_apigateway as apigateway
from aws_cdk import aws_lambda as lambda_

function = lambda_.Function(
    self,
    "MyLambda",
    function_name="my-lambda",
    runtime=lambda_.Runtime.PYTHON_3_10,
    handler="app.main.handler",  # Or the path to your handler function
    code=lambda_.Code.from_asset("./my_lambda"),  # Or the path to your code folder
    timeout=Duration.seconds(30),
    architecture=lambda_.Architecture.ARM_64,
    memory_size=1024,
)

auth = apigateway.CognitoUserPoolsAuthorizer(
    self,
    "MyCognitoAuthorizer",
    cognito_user_pools=[cognito_user_pool],  # Your Cognito User Pool you created
    authorizer_name="My-CognitoAuthorizer",
)

api = apigateway.LambdaRestApi(
    self,
    "MyApi",
    rest_api_name="my-api",
    handler=function,
    proxy=True,
    integration_options=apigateway.LambdaIntegrationOptions(
        allow_test_invoke=False,
    ),
    default_method_options=apigateway.MethodOptions(
        authorization_type=apigateway.AuthorizationType.COGNITO,
        authorizer=auth,
    ),
)

function.grant_invoke(iam.ServicePrincipal("apigateway.amazonaws.com"))
```