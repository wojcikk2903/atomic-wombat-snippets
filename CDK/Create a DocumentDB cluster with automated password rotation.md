# Create a DocumentDB cluster with automated password rotation

## Prerequisites

You need to create a VPC first and a security group. See [Create a VPC](./Create%20a%20VPC%20and%20a%20security%20group.md).

## Snippet

```py
from aws_cdk import aws_docdb as docdb
from aws_cdk import aws_ec2 as ec2

cluster = docdb.DatabaseCluster(
    self,
    "MyDocumentDBCluster",
    db_cluster_name="my-documentDB-cluster",
    master_user=docdb.Login(
        username="me",  # NOTE: 'admin' is reserved by DocumentDB
        secret_name="my-documentDB-secret",
    ),
    instance_type=ec2.InstanceType.of(
        ec2.InstanceClass.T4G, ec2.InstanceSize.MEDIUM
    ),
    vpc=vpc,
    vpc_subnets=ec2.SubnetSelection(
        subnet_type=ec2.SubnetType.PRIVATE_WITH_EGRESS
    ),
    security_group=my_security_group,
    storage_encrypted=True,
    backup=docdb.BackupProps(
        retention=Duration.days(7),
        preferred_window="01:00-02:00",
    ),
    preferred_maintenance_window="Sun:05:00-Sun:06:00",
    deletion_protection=True,
)

# Automatically rotate credentials in Secrets Manager every 30 days
cluster.add_rotation_single_user(automatically_after=Duration.days(30))
```