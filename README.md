# Atomic Wombat Snippets


Atomic Wombat Snippets is an open-source project that shares snippets of code that are handy or that can be reused across projects. We have snippets for a lot of different technologies (React, Nextjs, Fastapi, CDK, etc) and any new technology is welcome.

## Getting started

To get started with Atomic Wombat Snippets, you can simply browse this repository on Gitlab.

## Contributing

To contribute to Atomic Wombat Snippets, you need first to clone the repository

```
git clone git@gitlab.com:AlexBard/atomic-wombat-snippets.git
```

Then, simply open a merge request with your snippet. Please make sure that your snippet is well-documented and easy to use.

## License

Atomic Wombat Snippets is licensed under the MIT License. Feel free to use it any way you want.

## Thanks

Thank you to everyone who has contributed to Atomic Wombat Snippets!

## Examples

Here are a few examples of snippets that can be found in the repository:

* A snippet to autocomplete a cities field in React (using GoogleMaps API) -> [here](./React/Autocomplete%20cities%20field%20(with%20GoogleMaps%20API).md)
* A snippet to get password stored in AWS SecretsManager in Python -> [here](./Python/Get%20password%20from%20AWS%20SecretsManager.md)
* A snippet to handle multiple languages in Nextjs -> [here](./Nextjs/Use%20middleware%20to%20handle%20multiple%20languages.md)
* A snippet to add GraphQL to your Fastapi API -> [here](./Fastapi/Add%20GraphQL%20to%20your%20API.md)
* A snippet to deploy an API with AWS Lambda & API Gateway (with no authorization, with API Key or with CognitoAuthorizer) -> [here](./CDK/Deploy%20an%20API%20with%20Lambda%20&%20API%20Gateway%20(with%20API%20key%20or%20CognitoAuthorizer).md)

## How to use the snippets

To use a snippet, simply copy and paste it into your code. You may need to make some changes to the snippet to fit your specific needs (like variable names).

## Conclusion

Atomic Wombat Snippets is a great resource for finding code snippets that can be used in a variety of projects. If you are looking for a snippet, be sure to check out this repository!